package com.javatest.restful.service;

import com.javatest.restful.constant.ResponseConstant;
import com.javatest.restful.dto.RequestFindNumberForTarget;
import com.javatest.restful.dto.ResponseFindNumberForTarget;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class NumberService {

    public ResponseFindNumberForTarget findNumberForTarget(RequestFindNumberForTarget request) {
        ResponseFindNumberForTarget response = new ResponseFindNumberForTarget();

        try {

            if (this.validateInput(request)) {
                Integer target = request.getInputTarget();
                Set<Integer> listInputSet = request.getInputSet();

                List<List<Integer>> result = new ArrayList<>();

                for (Integer input : listInputSet) {

                    List<Integer> b = checkNumberCanBeTarget(target, input, listInputSet);

                    if (!b.isEmpty()) {
                        result.add(b);
                    }

                }

                response.setResult(result);
                response.setResponseStatus(ResponseConstant.RESPONSE.STATUS.SUCCESS);
                response.setResponseMessage(ResponseConstant.RESPONSE.MESSAGE.SUCCESS);
            } else {
                response.setResponseStatus(ResponseConstant.RESPONSE.STATUS.FAILURE);
                response.setResponseMessage("Input set or target is null");
            }

        } catch (Exception e) {
            response.setResponseStatus(ResponseConstant.RESPONSE.STATUS.FAILURE);
            response.setResponseMessage(e.getMessage());
        }

        return response;
    }

    private boolean validateInput(RequestFindNumberForTarget request) {
        return null != request.getInputSet() && (null != request.getInputTarget() && !request.getInputSet().isEmpty());
    }

    private List<Integer> checkNumberCanBeTarget(Integer target, Integer input, Set<Integer> listInputSet) {
        List<Integer> result = new ArrayList<>();

        Integer a = target / input;
        Integer b = target - (input * a);

        boolean hasNumberInSet = listInputSet.stream().anyMatch(t -> t.equals(b));

        int i = 0;
        while (i != a) {
            result.add(input);
            i++;
        }

        if (hasNumberInSet) {
            result.add(b);
        } else {
            if (target != (a * input)) {
                result.remove(input);
            }
        }

        return result;
    }

}
