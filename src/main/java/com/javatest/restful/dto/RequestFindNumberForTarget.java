package com.javatest.restful.dto;

import java.util.Set;

public class RequestFindNumberForTarget {
    private Set<Integer> inputSet;
    private Integer inputTarget;

    public Set<Integer> getInputSet() {
        return inputSet;
    }

    public void setInputSet(Set<Integer> inputSet) {
        this.inputSet = inputSet;
    }

    public Integer getInputTarget() {
        return inputTarget;
    }

    public void setInputTarget(Integer inputTarget) {
        this.inputTarget = inputTarget;
    }
}
