package com.javatest.restful.constant;

public class ResponseConstant {

    public static class RESPONSE {
        public static class STATUS {
            public final static String SUCCESS = "Success";
            public final static String FAILURE = "failure";
        }

        public static class MESSAGE {
            public final static String SUCCESS = "Success";
            public final static String FAILURE = "failure";
        }
    }
}
