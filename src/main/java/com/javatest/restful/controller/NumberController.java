package com.javatest.restful.controller;

import com.javatest.restful.constant.KeyMapConstant;
import com.javatest.restful.dto.RequestFindNumberForTarget;
import com.javatest.restful.dto.ResponseFindNumberForTarget;
import com.javatest.restful.service.NumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class NumberController {

    @Autowired
    private NumberService numberService;

    @ResponseBody
    @PostMapping(value = KeyMapConstant.NUMBER.FIND_NUMBER_FOR_TARGET.CONTROLLER, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseFindNumberForTarget findNumberForTarget(@Valid @RequestBody RequestFindNumberForTarget request) {
        return numberService.findNumberForTarget(request);
    }
}
