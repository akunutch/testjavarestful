package com.javatest.restful.constant;

public class KeyMapConstant {

    public static class NUMBER {
        public static class FIND_NUMBER_FOR_TARGET {
            public final static String CONTROLLER = "/findNumberForTarget";
        }
    }
}
