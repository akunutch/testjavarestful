package com.javatest.restful;

import com.javatest.restful.constant.ResponseConstant;
import com.javatest.restful.dto.RequestFindNumberForTarget;
import com.javatest.restful.dto.ResponseFindNumberForTarget;
import com.javatest.restful.service.NumberService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.Silent.class)
public class JavaTestApplicationTest {

    @Spy
    @InjectMocks
    private NumberService numberService;

    @Test
    public void CASE_SUCCESS_REQUEST_NULL() {
        ResponseFindNumberForTarget response = numberService.findNumberForTarget(mockInputForCaseSuccess());
        assertEquals(ResponseConstant.RESPONSE.STATUS.SUCCESS, response.getResponseStatus());
    }

    @Test
    public void CASE_FAILURE_REQUEST_NULL() {
        ResponseFindNumberForTarget response = numberService.findNumberForTarget(null);
        assertEquals(ResponseConstant.RESPONSE.STATUS.FAILURE, response.getResponseStatus());
    }

    @Test
    public void CASE_FAILURE_VALIDATE_INPUT() {
        ResponseFindNumberForTarget response = numberService.findNumberForTarget(mockInputForCaseValidateFail());
        assertEquals("Input set or target is null", response.getResponseMessage());
    }

    @Test
    public void CASE_FAILURE_VALIDATE_INPUT_2() {
        ResponseFindNumberForTarget response = numberService.findNumberForTarget(mockInputForCaseValidateFail2());
        assertEquals("Input set or target is null", response.getResponseMessage());
    }

    @Test
    public void CASE_FAILURE_VALIDATE_INPUT_3() {
        ResponseFindNumberForTarget response = numberService.findNumberForTarget(mockInputForCaseValidateFail3());
        assertEquals("Input set or target is null", response.getResponseMessage());
    }

    /* ======= Mock Area ======= */
    private RequestFindNumberForTarget mockInputForCaseSuccess() {
        Set<Integer> integerSet = new HashSet<>();
        integerSet.add(1);
        integerSet.add(3);

        RequestFindNumberForTarget request = new RequestFindNumberForTarget();
        request.setInputTarget(2);
        request.setInputSet(integerSet);
        return request;
    }

    private RequestFindNumberForTarget mockInputForCaseValidateFail() {
        RequestFindNumberForTarget request = new RequestFindNumberForTarget();
        return request;
    }

    private RequestFindNumberForTarget mockInputForCaseValidateFail2() {
        RequestFindNumberForTarget request = new RequestFindNumberForTarget();
        request.setInputTarget(2);
        return request;
    }

    private RequestFindNumberForTarget mockInputForCaseValidateFail3() {
        Set<Integer> integerSet = new HashSet<>();
        integerSet.add(1);
        integerSet.add(3);

        RequestFindNumberForTarget request = new RequestFindNumberForTarget();
        request.setInputSet(integerSet);
        return request;
    }
}
