package com.javatest.restful.dto;

import java.util.List;

public class ResponseFindNumberForTarget extends Response {
    private List<List<Integer>> result;

    public List<List<Integer>> getResult() {
        return result;
    }

    public void setResult(List<List<Integer>> result) {
        this.result = result;
    }
}
